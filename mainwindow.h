#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QListWidgetItem>
#include <QHBoxLayout>
#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QByteArray>
#include <QTcpSocket>
#include <QHostAddress>
#include <QUrlQuery>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <QNetworkInterface>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    bool ifSmbox(QByteArray str);
    QString DecToBin(int number);
    int BinToDec(int number);
    QString CDtoB(QString str);
    QString CBtoD(QString str);

    ~MainWindow();
    //QListWidget *listWidget;
    //QHBoxLayout *layout;

protected slots:
    void replyFinished(QNetworkReply *reply);
private:
    QNetworkInterface *inter;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
