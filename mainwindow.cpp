#include "mainwindow.h"
#include "ui_mainwindow.h"

int MainWindow::BinToDec(int number)
{
   if ((number)%10>1) return -1;
   if (number<10) return 1;
   else return (number % 10 + BinToDec(number/10)*2);
}
QString MainWindow::DecToBin(int number)
{
  if (number == 0) return "0";
  if (number == 1) return "1";
  if (number % 2 == 0) return DecToBin(number / 2) + "0";
  else return DecToBin(number / 2) + "1";
}
QString MainWindow::CDtoB(QString str)
{
    QString bin,binrez;
    int k=0;
    str.append('.');
    for (int i=0;i<str.length();i++)
    {
        if (str[i]=='.'){
            bin="";
            for (int j=k;j<i;j++) bin.append(str[j]);
            if (DecToBin(bin.toInt()).length()<8)
            {
                for (int j=0;j<8-DecToBin(bin.toInt()).length();j++) binrez.append('0');
            }
            binrez.append(DecToBin(bin.toInt()));
            binrez.append('.');
            k=i+1;
        }
    }
    binrez.chop(1);
    return binrez;
}
QString MainWindow::CBtoD(QString str)
{
    QString bin,binrez;
    int k=0;
    str.append('.');
    for (int i=0;i<str.length();i++)
    {
        if (str[i]=='.'){
            bin="";
            for (int j=k;j<i;j++) bin.append(str[j]);
            if (bin.toInt()!=0) binrez.append(QString::number(BinToDec(bin.toInt())));
            else binrez.append('0');
            binrez.append('.');
            k=i+1;
        }
    }
    binrez.chop(1);
    return binrez;
}

bool MainWindow::ifSmbox(QByteArray str)
{
    if (str.startsWith("SmBox")) return true;
    else return false;
}
void MainWindow::replyFinished(QNetworkReply *reply)
{
    QByteArray str;
    str=reply->readAll();
    if (ifSmbox(str)) ui->listWidget_2->addItem(str);
    reply->deleteLater();
}


//===================================================================================================================================================


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);     

    QList<QHostAddress> listIpAddrs;
    QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();
    QList<QNetworkInterface> listInterNames;
    inter=new QNetworkInterface();
    listIpAddrs = inter->allAddresses();
    listInterNames = inter->allInterfaces();
    QString str;

    ui->listWidget->addItem(new QListWidgetItem("IP addresses:") );
    for (int i = 0; i < listInterNames.size(); i++)
       {
        QNetworkInterface iface = ifaces.at(i);
            ui->listWidget->addItem(new QListWidgetItem("Inet: " +iface.addressEntries().at(0).ip().toString()));
            ui->listWidget->addItem(new QListWidgetItem("Netmask: "+iface.addressEntries().at(0).netmask().toString()));
            ui->listWidget->addItem(new QListWidgetItem("broadcast: "+iface.addressEntries().at(0).broadcast().toString()));
            ui->listWidget->addItem(new QListWidgetItem(QString("\n")));

       }
    ui->listWidget->addItem(new QListWidgetItem("Interface names:") );
    for (int i = 0; i < listInterNames.size(); ++i)
       {
       str = listInterNames.at(i).name();  // Show the IP address
       ui->listWidget->addItem(new QListWidgetItem(str));
       }

    //===============================================================================================================================================

    QString ip,dm,bin,bm,ipmin,ipmax;
    QNetworkInterface iface = ifaces.at(2);
    ip=iface.addressEntries().at(0).ip().toString();
    dm=iface.addressEntries().at(0).netmask().toString();
    bin=CDtoB(ip);
    bm=CDtoB(dm);
    ui->listWidget->addItem(bin);
    //ui->listWidget->addItem(bin);
    for(int j=0;j<=bin.length();j++)
    {
        if (bin[j]=='1')
            if (bm[j]=='0')
                bin[j]='0';
    }
    ipmin=CBtoD(bin);
    ui->listWidget_2->addItem(ipmin);
    //ui->listWidget_2->addItem(QString(" "));
    bin=CDtoB(ip);
    for(int j=0;j<=bin.length();j++)
    {
        if (bin[j]=='0')
            if (bm[j]=='0')
                bin[j]='1';
    }
    ipmax=CBtoD(bin);
    ui->listWidget_2->addItem(ipmax);
    ui->listWidget_2->addItem(QString(" "));

    //===============================================================================================================================================

    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(ui->listWidget);
    QString s[4],sk;
    QString z[4];
    QNetworkRequest request;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
                  this, SLOT(replyFinished(QNetworkReply*)));

    ipmin.append('.');
    ipmax.append('.');
    int k=0,q=0;
    for (int i=0;i<ipmin.length();i++)
    {
        if (ipmin[i]=='.')
        {
            bin="";
            for (int j=k;j<i;j++) bin.append(ipmin[j]);
            s[q]=bin;
            q++;
            k=i+1;
        }
    }
    q=0; k=0;
    for (int i=0;i<ipmax.length();i++)
    {

        if (ipmax[i]=='.')
        {
            bin="";
            for (int j=k;j<i;j++) bin.append(ipmax[j]);
            z[q]=bin;
            q++;
            k=i+1;
        }
    }
    ipmin.chop(1);
    ipmax.chop(1);


    int i=0,y=0,t=0,g=0;
    for (i=s[0].toInt();i<=z[0].toInt();i++){
        for (y=s[1].toInt();y<=z[1].toInt();y++){
            for(t=s[2].toInt();t<=z[2].toInt();t++){
                for(g=s[3].toInt();g<=z[3].toInt();g++){
                    s[3]=QString::number(g);
                    sk="http://";
                    sk=sk+s[0]+'.'+s[1]+'.'+s[2]+'.'+s[3];
                    sk=sk+":9898/scanid";
                    request = QNetworkRequest(QUrl(sk));
                    manager->get(request);
                }
            }
        }
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}
